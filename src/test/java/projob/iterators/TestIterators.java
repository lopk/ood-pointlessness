package projob.iterators;

import org.junit.jupiter.api.Test;
import projob.iterators.implementation.VectorArray;
import projob.iterators.implementation.VectorRange;
import projob.iterators.implementation.VectorSparse;
import projob.iterators.utils.AbstractVectorUtils;

import java.util.ArrayList;
import java.util.List;

import static projob.iterators.utils.AbstractVectorUtils.*;


public class TestIterators {

	@Test
	//To nie jest test
	public void test_main_functionality() {
        List<AbstractVector<Double>> vectors = new ArrayList<>();
        vectors.add(new VectorRange(0, 10));
        vectors.add(new VectorSparse(5));
        vectors.add(new VectorArray(new double[]{0.1, 2.2, 0.0, 5.5, -50}));
        vectors.add(new VectorRange(-10, 5));
        vectors.add(new VectorArray(new double[]{10, 9, 8, 7, 6}));
        vectors.add(VectorizedConcatenation(vectors.get(0), vectors.get(1)));
        vectors.add(VectorizedMinimum(vectors.get(0), vectors.get(2)));
        vectors.add(new VectorSparse(VectorizedSum(vectors.get(2), vectors.get(3))));
        vectors.add(new VectorRange(VectorizedConcatenation(vectors.get(3), vectors.get(4))));
        vectors.stream()
                .peek(AbstractVectorUtils::PrintVector)
                .forEach(System.out::println);
        PrintVector(vectors.get(5), System.err);
    }
}
