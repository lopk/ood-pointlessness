package projob.iterators.implementation;

import org.junit.jupiter.api.Test;
import projob.iterators.AbstractVector;

import java.util.Arrays;
import java.util.InputMismatchException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class VectorArrayTest {
	private static final double[] VALUES = new double[]{0.0, Double.NaN, Double.NEGATIVE_INFINITY,
			Double.POSITIVE_INFINITY, Double.MAX_VALUE, Double.MIN_VALUE};

	@Test
	void getLength() {
		AbstractVector<Double> vector = new VectorArray(VALUES);
		assertEquals(VALUES.length, vector.getLength());
	}

	@Test
	void initializeFrom() {
		Double[] valuesDouble = new Double[VALUES.length];
		for (int i = 0; i < VALUES.length; i++) {
			valuesDouble[i] = VALUES[i];
		}
		AbstractVector<Double> vector = new VectorArray(Arrays.asList(valuesDouble));
		int idx = 0;
		for (Double d : vector)
			assertEquals(VALUES[idx++], d);
		//Needed to pass empty iterable to the constructor of VectorArray
		//noinspection UnnecessaryBoxing
		assertThrows(InputMismatchException.class, () -> new VectorArray(Arrays.asList(null, Double.valueOf(10))));
	}

	@Test
	void iterator() {
		AbstractVector<Double> vector = new VectorArray(VALUES);
		int idx = 0;
		for (Double d : vector)
			assertEquals(VALUES[idx++], d);
	}
}