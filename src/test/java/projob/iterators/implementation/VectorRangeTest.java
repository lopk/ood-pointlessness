package projob.iterators.implementation;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import projob.iterators.AbstractVector;

import java.util.Arrays;
import java.util.InputMismatchException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class VectorRangeTest {
	private static final double[] VALUES = new double[]{0.0, Double.NaN, Double.NEGATIVE_INFINITY,
			Double.POSITIVE_INFINITY, Double.MAX_VALUE, Double.MIN_VALUE};

	@ParameterizedTest
	@CsvSource({"0,0,false",
			"1,-1,false",
			"100,-1,false",
			"-2147483648,2147483647,true"})
	void getLength(int start, int end, boolean shouldThrow) {
		if (shouldThrow) {
			System.out.println(start + " " + end);
			assertThrows(InputMismatchException.class, () -> System.out.println(new VectorRange(start, end).getLength()), "Should throw when overflow can occur.");
		} else {
			AbstractVector<Double> vector = new VectorRange(start, end);
			assertEquals(Math.abs(end - start) + 1, vector.getLength());
		}
	}

	@Test
	void initializeFrom() {
		Double[] valuesDouble = new Double[VALUES.length];
		for (int i = 0; i < VALUES.length; i++) {
			valuesDouble[i] = VALUES[i];
		}
		assertThrows(InputMismatchException.class, () -> new VectorRange(Arrays.asList(valuesDouble)), "Should not allow non linear values");
	}

	@Test
	void iterator() {
		AbstractVector<Double> vector = new VectorArray(VALUES);
		int idx = 0;
		for (Double d : vector)
			assertEquals(VALUES[idx++], d);
	}
}