package projob.iterators.implementation;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import projob.iterators.AbstractVector;

import java.util.Arrays;
import java.util.Collections;
import java.util.InputMismatchException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class VectorSparseTest {
	private static final double[] VALUES = new double[]{0.0, Double.NaN, Double.NEGATIVE_INFINITY,
			Double.POSITIVE_INFINITY, Double.MAX_VALUE, Double.MIN_VALUE};

	@Test
	void addValue() {
		VectorSparse vectorSparse = new VectorSparse(5);
		vectorSparse.addValue(0, 5);
		vectorSparse.addValue(1, 99);
		vectorSparse.addValue(2, 65);
		vectorSparse.addValue(3, Double.NaN);
		assertThrows(InputMismatchException.class, () -> vectorSparse.addValue(-1, Double.NaN),
				"Shouldn't allow bad input");
	}

	@ParameterizedTest
	@ValueSource(ints = {-20, 0, 1, 2 << 10, 3 << 17, 2147483647})
	void getLength(int len) {
		if (len <= 0) {
			assertThrows(InputMismatchException.class, () -> new VectorSparse(len),
					"Shouldn't allow non positive size");
			return;
		}
		VectorSparse vectorSparse = new VectorSparse(len);
		assertEquals(len, vectorSparse.getLength());
	}

	@Test
	void initializeFrom() {
		Double[] valuesDouble = new Double[VALUES.length];
		for (int i = 0; i < VALUES.length; i++) {
			valuesDouble[i] = VALUES[i];
		}
		AbstractVector<Double> vector = new VectorSparse(Arrays.asList(valuesDouble));
		int idx = 0;
		for (Double d : vector)
			assertEquals(VALUES[idx++], d);
		assertThrows(InputMismatchException.class, () -> new VectorSparse(Collections.emptyList()), "Should reject empty iterable.");
	}

	@Test
	void iterator() {
		VectorSparse vectorSparse = new VectorSparse(5);
		vectorSparse.addValue(0, 29);
		vectorSparse.addValue(1, -98);
		vectorSparse.addValue(4, Double.POSITIVE_INFINITY);
		double[] expected = new double[]{29, -98, 0, 0, Double.POSITIVE_INFINITY};
		int idx = 0;
		for (Double d : vectorSparse)
			assertEquals(expected[idx++], d);
	}
}