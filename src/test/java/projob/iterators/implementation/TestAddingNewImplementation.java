package projob.iterators.implementation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

final class TestAddingNewImplementation {

	//This test is manual
	//Uncomment the following static class and check what is the minimum to be implemented

//	static class HowToDestroy extends AbstractVector<Double> {
//
//	}

	@Test
	void testConstructor() {
		//This test is purely manual. It checks weather java enforces implementing constructor of
		//this signature ClassName(Iterable<T> vector)

		//AbstractVector<Double> vector = new HowToDestroy();
		//AbstractVector<Double> vector = new HowToDestroy(new VectorArray(new double[]{1,2,3,4}));

		//After checking both options I chose option where compiler enforces having the specified above constructor
		assertTrue(true);
	}
}
