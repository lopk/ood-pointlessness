package projob.iterators.utils;

import org.junit.jupiter.api.Test;
import projob.iterators.implementation.VectorArray;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AbstractVectorUtilsTest {

	@Test
	void printVector() {
		var values = new Double[]{(double) -1, (double) -20, 5d, (double) 0};
		assertEquals("-1.0, -20.0, 5.0, 0.0", AbstractVectorUtils.VectorToString(Arrays.asList(values)));
	}

	@Test
	void vectorizedCombine() {
		var res = AbstractVectorUtils.VectorizedMinimum(new VectorArray(new double[]{1, 2}), new VectorArray(new double[]{-1, -2, 4, 5}));
		var expected = new double[]{-1, -2, 1, 2};
		int i = 0;
		for (Double d :
				res) {
			assertEquals(expected[i++], d);
		}
	}

	@Test
	void vectorizedConcatenation() {
		var res = AbstractVectorUtils.VectorizedConcatenation(new VectorArray(new double[]{1, 2}), new VectorArray(new double[]{-1, -2, 4, 5}));
		var expected = new double[]{1, 2, 5, 4, -2, -1};
		int i = 0;
		for (Double d :
				res) {
			assertEquals(expected[i++], d);
		}
	}
}