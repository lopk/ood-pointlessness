package projob.decorators;

import projob.decorators.istreamimplementation.StringStream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

public class TestStringStream {

    @ParameterizedTest
    @EmptySource
    @ValueSource(strings = {" ", "   ", "\t", "\n", "Hello World"})
    public void test_readNextAndCheckAtEnd(String s) {
        StringStream stringStream = new StringStream(s);
        for (int i = 0; i < s.length(); ++i) {
            assertFalse(stringStream.atEnd());
            assertSame(s.charAt(i), stringStream.readNext());
        }
        assertTrue(stringStream.atEnd());
    }
}