package projob.decorators;


import projob.decorators.istreamdecoratorimplementation.*;
import projob.decorators.istreamimplementation.StringStream;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @noinspection SpellCheckingInspection
 */
public class TestIStreamDecorator {
    /**
     *
     */
    private final String s1 = "Faculty of Mathematics and Information Science, Warsaw University of Technology";
    private final String s2 = " is the best place to learn about object oriented design. Period.";
    private final String s3 = "Lrmismdlrstae,cnettraiicn lt tfclssagea au cusntitqencvteoi.";
    private final String s4 = "oe pu oo i mt osceu dpsigei.U aiii uu clcsacma rsiu e ia do";


    @Test
    public void test_to_upper() {
        IStream stream = new UpperCaseStreamDecorator(new StringStream(s1));
        testStream(s1.toUpperCase(), stream);
    }

    @Test
    public void test_to_lower() {
        IStream stream = new LowerCaseStreamDecorator(new StringStream(s1));
        testStream(s1.toLowerCase(), stream);
    }

    @Test
    public void test_underscore() {
        IStream stream = new UnderscoreStreamDecorator(new StringStream(s1));
        testStream(s1.replace(' ', '_'), stream);
    }

    @Test
    public void test_left_right() {
        IStream streamLeft = new StringStream(s3);
        IStream streamRight = new StringStream(s4);

        IStream decorated = new MetronomeStreamDecorator(streamLeft, streamRight);
        testStream("Lorem ipsum dolor sit amet, consectetur adipiscing elit. " +
                "Ut facilisis augue ac lacus accumsan tristique nec vitae odio", decorated);
    }

    @Test
    public void test_join() {
        IStream st1 = new StringStream(s1);
        IStream st2 = new StringStream(s2);
        IStream st3 = new StringStream(s3);
        IStream st4 = new StringStream(s4);
        IStream st5 = new UpperCaseStreamDecorator(new StringStream(s1));
        IStream st6 = new MetronomeStreamDecorator(new StringStream(s3), new StringStream(s4));
        IStream decorated = new JoinStreamDecorator(st1, st2, st3, st4, st5, st6);
        System.out.println(decorated.AsString());
    }

    @Test
    public void test_pivot_join() {
        IStream st1 = new StringStream(".");
        IStream st2 = new StringStream("h");
        IStream st3 = new StringStream("a");
        IStream st4 = new StringStream("r");

        IStream decorated = new PivotJoinStreamDecorator('k', st1, st2, st3, st4);
        testStream("h", decorated);
    }

    @Test
    public void test_combining() {
        IStream stream = new StringStream(s1);
        IStream another = new StringStream(s2);

        IStream decorated = new JoinStreamDecorator(new LowerCaseStreamDecorator(stream), new UpperCaseStreamDecorator(another));
        System.out.println(decorated.AsString());
    }

    @Test
    public void test_sentence_decorator() {
        IStream stream = new StringStream(s2);
        IStream decorated = new SentenceStreamDecorator(stream);
        String s = decorated.AsString();
        System.out.println(s);
        assertTrue(s.contains("Period"));
    }

    @Test
    public void test_caezar_cipher() {
        IStream stream = new StringStream(s1);
        testStream(s1, new CaesarDecryptStreamDecorator(new CaesarEncryptStreamDecorator(stream)));

        stream = new CaesarEncryptStreamDecorator(new StringStream("abc"));
        testStream("def", stream);

        stream = new CaesarDecryptStreamDecorator(new StringStream("def"));
        testStream("abc", stream);
    }

    @Test
    public void test_upper_lower_upper_lower() {
        IStream stream = new StringStream("abcde");
        testStream("AbCdE", new MetronomeCaseStreamDecorator(stream));
    }

    @Test
    public void test_first_n() {
        IStream stream = new StringStream("Hello World");
        testStream("Hello", new FirstNStringDecorator(5, stream));
    }

    @Test
    public void test_skip_n() {
        IStream stream = new StringStream("Hello World");
        testStream("World", new SkipNStringDecorator(6, stream));
    }

    public void testStream(String expected, IStream stream) {
        assertFalse(stream.atEnd());
        int i = 0;
        while (!stream.atEnd()) {
            char c = stream.readNext();
            assertSame(expected.charAt(i++), c);
            System.out.print(c);
        }
        System.out.println();
    }
}
