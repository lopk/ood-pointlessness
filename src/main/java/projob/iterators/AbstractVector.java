package projob.iterators;

import java.util.InputMismatchException;

public abstract class AbstractVector<T> implements Iterable<T> {

	//Constructor requirements
	protected AbstractVector(@SuppressWarnings("unused") Iterable<T> vector) throws InputMismatchException {
	}

	//Interface
	public abstract int getLength();
}
