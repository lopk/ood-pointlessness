package projob.iterators.implementation;

import projob.iterators.AbstractVector;

import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.Map;

public class VectorSparse extends AbstractVector<Double> {
    private Map<Integer, Double> fields;
    private int length;

    public VectorSparse(Iterable<Double> vector) throws InputMismatchException {
		super(vector);
		if (vector == null) throw new InputMismatchException("I don't accept null parameter");
		Map<Integer, Double> fields = new HashMap<>();
		int length = 0;
		for (double d : vector) {
			if (d != 0) fields.put(length, d);
			++length;
		}
		if (length == 0) throw new InputMismatchException("I don't accept empty vectors, since I must know my length.");
		this.length = length;
		this.fields = fields;
	}

	public VectorSparse(int length) throws InputMismatchException {
		super(null);
		if (length <= 0) throw new InputMismatchException("I don't accept non positive length");
		this.length = length;
		fields = new HashMap<>();
	}

	public void addValue(int index, double value) {
		if (index < 0) throw new InputMismatchException("I don't allow negative index values");
		if (index < getLength() && value != 0)
			fields.put(index, value);
		if (index < getLength() && value == 0)
			fields.remove(index);
	}

	@Override
	public int getLength() {
		return this.length;
	}

    @Override
    public Iterator<Double> iterator() {
        return new Iterator<>() {
			private int currIdx = 0;

            @Override
            public boolean hasNext() {
				return currIdx < length;
            }

            @Override
            public Double next() {
				Double val = fields.getOrDefault(currIdx, 0.0);
				++currIdx;
				return val;
			}
        };
    }
}