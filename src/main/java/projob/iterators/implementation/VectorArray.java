package projob.iterators.implementation;

import projob.iterators.AbstractVector;

import java.util.InputMismatchException;
import java.util.Iterator;

public class VectorArray extends AbstractVector<Double> {
    private double[] array;

    public VectorArray(Iterable<Double> vector) throws InputMismatchException {
		super(vector);
		if (vector == null) throw new InputMismatchException("I don't accept null parameter");
		int length = 0;
		for (Double ignored : vector) {
			if (ignored == null) throw new InputMismatchException("I don't support null values");
			if (length + 1 < length)
				throw new InputMismatchException("I don't support vectors of size more than max int");
			++length;
		}
		this.array = new double[length];
		var iter = vector.iterator();
		for (int i = 0; i < length; ++i) {
			if (!iter.hasNext())
				throw new InputMismatchException("Parameter vector must not change during this initializations");
			this.array[i] = iter.next();
		}
	}

    public VectorArray(double[] values) {
		super(null);
		array = new double[values.length];
		System.arraycopy(values, 0, array, 0, array.length);
	}

	@Override
	public int getLength() {
		return array.length;
	}

    @Override
    public Iterator<Double> iterator() {
        return new Iterator<>() {
			private int position = 0;

            @Override
            public boolean hasNext() {
				return position < array.length;
            }

            @Override
            public Double next() {
				return array[position++];
            }
        };
    }
}