package projob.iterators.implementation;

import projob.iterators.AbstractVector;

import java.util.InputMismatchException;
import java.util.Iterator;

public class VectorRange extends AbstractVector<Double> {
    private int start, end;

    public VectorRange(Iterable<Double> vector) throws InputMismatchException {
		super(vector);
		if (vector == null) throw new InputMismatchException("I don't accept null parameter");
		int start, end;
		Iterator<Double> iterator = vector.iterator();
		if (!iterator.hasNext()) throw new InputMismatchException("I don't support empty vector");
		start = iterator.next().intValue();
		end = start;
		if (!iterator.hasNext()) {
			this.SetState(start, end);
			return;
		}
		end = iterator.next().intValue();
		if (end == start) throw new InputMismatchException("I don't support repeating values");
		boolean incr = end > start;
		for (int d; iterator.hasNext(); ) {
			d = iterator.next().intValue();
			if (d - end != (incr ? 1 : -1))
				throw new InputMismatchException("I don't support unordered vectors");
			end = d;
		}
		this.SetState(start, end);
	}

	public VectorRange(int start, int end) throws InputMismatchException {
		super(null);
		this.SetState(start, end);
	}

	@Override
	public int getLength() {
		return Math.abs(end - start) + 1;
	}

	private void SetState(int start, int end) throws InputMismatchException {
		long len = Math.abs((long) start - end) + 1;
		if (len > Integer.MAX_VALUE)
			throw new InputMismatchException("I don't support case, when length > Integer.MAX_VALUE");
		this.start = start;
		this.end = end;
	}

    @Override
    public Iterator<Double> iterator() {
        return new Iterator<>() {
			private Integer current = start;

            @Override
            public boolean hasNext() {
                return current != null;
            }

            @Override
            public Double next() {
                var ret = current.doubleValue();
				if (current == end) {
					current = null;
					return ret;
				}
				current += (end >= start ? 1 : -1);
                return ret;
            }
        };
    }
}