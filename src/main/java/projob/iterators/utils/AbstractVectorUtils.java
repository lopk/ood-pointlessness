package projob.iterators.utils;

import projob.iterators.AbstractVector;
import projob.iterators.implementation.VectorArray;

import java.io.PrintStream;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.function.BiFunction;


public final class AbstractVectorUtils {
	private AbstractVectorUtils() {
	}

	public static void PrintVector(Iterable<?> vector) throws InputMismatchException {
		PrintVector(vector, System.out);
	}

	public static void PrintVector(Iterable<?> vector, PrintStream out) throws InputMismatchException {
		if (out == null) throw new InputMismatchException("Input parameters can not be null");
		out.println(VectorToString(vector));
	}

	public static String VectorToString(Iterable<?> vector) throws InputMismatchException {
		if (vector == null) throw new InputMismatchException("I don't accept null as vector");
		StringBuilder sb = new StringBuilder();
		Iterator<?> enu = vector.iterator();
		if (!enu.hasNext()) return "";
		sb.append(enu.next().toString());
		for (; enu.hasNext(); )
			sb.append(", ").append(enu.next().toString());
		return sb.toString();
	}

	public static VectorArray VectorizedMinimum(AbstractVector<Double> a, AbstractVector<Double> b)
			throws InputMismatchException {
		return new VectorArray(VectorizedCombine(a, b, Math::min));
	}

	public static Iterable<Double> VectorizedCombine(AbstractVector<Double> a, AbstractVector<Double> b, BiFunction<Double, Double, Double> func)
			throws InputMismatchException {
		if (a == null && b == null)
			throw new InputMismatchException("At least one incoming vector must not be null");
		if (a == null || b == null)
			return a == null ? b : a;
		return new Iterable<>() {
			private final Iterable<Double> left = a;
			private final Iterable<Double> right = b;
			private final BiFunction<Double, Double, Double> function = func;

			@Override
			public Iterator<Double> iterator() {
				return new Iterator<>() {
					private Iterator<Double> ai = left.iterator();
					private Iterator<Double> bi = right.iterator();

					private boolean aAlreadyThrough = false;
					private boolean bAlreadyThrough = false;

					@Override
					public boolean hasNext() {
						return (!aAlreadyThrough && ai.hasNext()) || (!bAlreadyThrough && bi.hasNext());
					}

					@Override
					public Double next() {
						if (!ai.hasNext() && !bAlreadyThrough) {
							ai = left.iterator();
							aAlreadyThrough = true;
						} else if (!aAlreadyThrough && !bi.hasNext()) {
							bi = right.iterator();
							bAlreadyThrough = true;
						}
						return function.apply(ai.next(), bi.next());
					}
				};
			}
		};
	}

    public static VectorArray VectorizedConcatenation(AbstractVector<Double> a, AbstractVector<Double> b)
    {
		double[] doubles = new double[a.getLength() + b.getLength()];
		int cnt = 0;
        for (double d: a ) {
            doubles[cnt++] = d;
        }
		if (a.getLength() != cnt)
			throw new IllegalStateException();
        cnt=0;
        for (double d: b ) {
            doubles[doubles.length-++cnt] = d;
        }
        return new VectorArray(doubles);
    }

    public static Iterable<Double> VectorizedSum(AbstractVector<Double> a, AbstractVector<Double> b) {

		return VectorizedCombine(a, b, Double::sum);
	}
}
