package projob.decorators;

public abstract class IStreamDecorator implements IStream {
    protected final IStream component;

    protected IStreamDecorator(IStream component) {
        this.component = component;
    }


    @Override
    public boolean atEnd() {
        return this.component.atEnd();
    }
}
