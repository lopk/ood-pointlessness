package projob.decorators;

public interface IStream {
    char readNext();

    boolean atEnd();

    default String AsString() {
        StringBuilder sb = new StringBuilder();
        while (!this.atEnd()) sb.append(this.readNext());
        return sb.toString();
    }
}
