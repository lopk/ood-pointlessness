package projob.decorators.istreamdecoratorimplementation;

import projob.decorators.IStream;

public class SkipNStringDecorator implements IStream {
    private final IStream stream;

    public SkipNStringDecorator(int N, IStream stream) {
        this.stream = stream;
        while (N-- > 0) {
            if (stream.atEnd()) continue;
            stream.readNext();
        }
    }

    @Override
    public char readNext() {
        return stream.readNext();
    }

    @Override
    public boolean atEnd() {
        return stream.atEnd();
    }
}
