package projob.decorators.istreamdecoratorimplementation;

import projob.decorators.IStream;

public class CaesarEncryptStreamDecorator implements IStream {
    /**
     * @noinspection SpellCheckingInspection
     */
    private static final String ALPHABET = "abcdefghijklmnopqrstuvwxyz";
    private final IStream stream;

    public CaesarEncryptStreamDecorator(IStream stream) {
        this.stream = stream;
    }

    private static char encrypt(char c) {
        int charPosition = ALPHABET.indexOf(c);
        int keyVal = (3 + charPosition) % 26;
        return ALPHABET.charAt(keyVal);
    }

    @Override
    public char readNext() {
        char c = stream.readNext();
        if (ALPHABET.indexOf(Character.toLowerCase(c)) != -1) {
            if (Character.isUpperCase(c)) {
                return Character.toUpperCase(encrypt(Character.toLowerCase(c)));
            }
            return encrypt(c);
        }
        return c;
    }

    @Override
    public boolean atEnd() {
        return stream.atEnd();
    }
}
