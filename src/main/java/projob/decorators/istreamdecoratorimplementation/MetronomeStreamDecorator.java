package projob.decorators.istreamdecoratorimplementation;

import projob.decorators.IStream;

public class MetronomeStreamDecorator implements IStream {

    private final IStream streamLeft;
    private final IStream streamRight;
    private int cnt;

    public MetronomeStreamDecorator(IStream streamLeft, IStream streamRight) {
        this.streamLeft = streamLeft;
        this.streamRight = streamRight;
        cnt = 0;
    }

    @Override
    public char readNext() {
        return 0 == cnt++ % 2 ? streamLeft.readNext() : streamRight.readNext();
    }

    @Override
    public boolean atEnd() {
        return 0 == cnt ? streamLeft.atEnd() : streamRight.atEnd();
    }
}
