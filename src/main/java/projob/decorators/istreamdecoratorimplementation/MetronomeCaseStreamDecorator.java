package projob.decorators.istreamdecoratorimplementation;

import projob.decorators.IStream;

public class MetronomeCaseStreamDecorator implements IStream {
    private final IStream stream;
    private int cnt = 0;

    public MetronomeCaseStreamDecorator(IStream stream) {
        this.stream = stream;
    }

    @Override
    public char readNext() {
        return cnt++ % 2 == 0 ? Character.toUpperCase(stream.readNext()) : Character.toLowerCase(stream.readNext());
    }

    @Override
    public boolean atEnd() {
        return stream.atEnd();
    }
}
