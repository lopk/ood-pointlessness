package projob.decorators.istreamdecoratorimplementation;

import projob.decorators.IStream;

public class CaesarDecryptStreamDecorator implements IStream {
    /**
     * @noinspection SpellCheckingInspection
     */
    private static final String ALPHABET = "abcdefghijklmnopqrstuvwxyz";
    private final IStream encryptedStream;

    public CaesarDecryptStreamDecorator(IStream encryptedStream) {
        this.encryptedStream = encryptedStream;
    }

    private static char decrypt(char c) {
        int charPosition = ALPHABET.indexOf(c);
        int keyVal = (ALPHABET.length() - 3 + charPosition) % ALPHABET.length();
        return ALPHABET.charAt(keyVal);
    }

    @Override
    public char readNext() {
        char c = encryptedStream.readNext();
        if (ALPHABET.indexOf(Character.toLowerCase(c)) != -1) {
            if (Character.isUpperCase(c)) {
                return Character.toUpperCase(decrypt(Character.toLowerCase(c)));
            }
            return decrypt(c);
        }
        return c;
    }

    @Override
    public boolean atEnd() {
        return encryptedStream.atEnd();
    }
}
