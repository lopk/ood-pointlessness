package projob.decorators.istreamdecoratorimplementation;

import projob.decorators.IStream;
import projob.decorators.IStreamDecorator;

public class UnderscoreStreamDecorator extends IStreamDecorator {
    public UnderscoreStreamDecorator(IStream stream) {
        super(stream);
    }

    @Override
    public char readNext() {
        char c = component.readNext();
        return c != ' ' ? c : '_';
    }
}
