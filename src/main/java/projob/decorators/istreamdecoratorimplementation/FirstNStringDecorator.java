package projob.decorators.istreamdecoratorimplementation;

import projob.decorators.IStream;

public class FirstNStringDecorator implements IStream {
    private final IStream stream;
    private int N;

    public FirstNStringDecorator(int N, IStream stream) {
        this.N = N;
        this.stream = stream;
    }

    @Override
    public char readNext() {
        --N;
        return stream.readNext();
    }

    @Override
    public boolean atEnd() {
        return N <= 0;
    }
}
