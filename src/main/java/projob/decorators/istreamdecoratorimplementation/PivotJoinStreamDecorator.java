package projob.decorators.istreamdecoratorimplementation;

import projob.decorators.IStream;

import java.util.Objects;
import java.util.stream.Stream;

public class PivotJoinStreamDecorator implements IStream {
    private final char pivot;
    private final IStream[] streams;
    private final Character[] current;

    public PivotJoinStreamDecorator(char pivot, IStream... streams) {
        this.streams = streams;
        this.pivot = pivot;
        this.current = new Character[this.streams.length];
    }

    @Override
    public char readNext() {
        for (int i = 0; i < streams.length; ++i) {
            if (streams[i].atEnd()) current[i] = null;
            else current[i] = streams[i].readNext();
        }
        //noinspection OptionalGetWithoutIsPresent
        return Stream.of(current).filter(Objects::nonNull).reduce((a, b) -> {
            if (Math.abs(a - pivot) <= Math.abs(b - pivot)) return a;
            else return b;
        }).get();
    }

    @Override
    public boolean atEnd() {
        return Stream.of(streams).allMatch(IStream::atEnd);
    }
}
