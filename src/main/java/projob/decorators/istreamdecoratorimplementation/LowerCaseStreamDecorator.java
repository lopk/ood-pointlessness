package projob.decorators.istreamdecoratorimplementation;

import projob.decorators.IStream;
import projob.decorators.IStreamDecorator;

public class LowerCaseStreamDecorator extends IStreamDecorator {
    public LowerCaseStreamDecorator(IStream component) {
        super(component);
    }

    @Override
    public char readNext() {
        return Character.toLowerCase(component.readNext());
    }
}
