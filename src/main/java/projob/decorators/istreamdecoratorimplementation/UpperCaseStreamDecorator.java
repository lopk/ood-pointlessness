package projob.decorators.istreamdecoratorimplementation;

import projob.decorators.IStream;
import projob.decorators.IStreamDecorator;

public class UpperCaseStreamDecorator extends IStreamDecorator {

    public UpperCaseStreamDecorator(IStream component) {
        super(component);
    }

    @Override
    public char readNext() {
        return Character.toUpperCase(component.readNext());
    }
}
