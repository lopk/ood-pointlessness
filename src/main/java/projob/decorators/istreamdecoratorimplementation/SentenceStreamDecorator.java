package projob.decorators.istreamdecoratorimplementation;

import projob.decorators.IStream;

public class SentenceStreamDecorator implements IStream {

    private final IStream stream;
    private boolean change = false;

    public SentenceStreamDecorator(IStream stream) {
        this.stream = stream;
    }

    @Override
    public char readNext() {
        char c = stream.readNext();
        if (c == '.') {
            this.change = true;
            return c;
        }
        if (change && Character.isLetter(c)) {
            change = false;
            return Character.toUpperCase(c);
        }
        return c;
    }

    @Override
    public boolean atEnd() {
        return stream.atEnd();
    }
}
