package projob.decorators.istreamdecoratorimplementation;

import projob.decorators.IStream;

public class JoinStreamDecorator implements IStream {
    private final IStream[] streams;
    private int index;

    public JoinStreamDecorator(IStream... streams) {
        this.streams = streams;
    }

    @Override
    public char readNext() {
        while (streams[index].atEnd()) ++index;
        return streams[index].readNext();
    }

    @Override
    public boolean atEnd() {
        return checkEnd(index);
    }

    private boolean checkEnd(int index) {
        if (index == streams.length) return true;
        else {
            return streams[index].atEnd() && checkEnd(index + 1);
        }
    }
}
