package projob.decorators.istreamimplementation;

import projob.decorators.IStream;

public class StringStream implements IStream {
    private final String value;
    private int alreadyReadChars = 0;

    public StringStream(String value) {
        this.value = value;
    }

    @Override
    public char readNext() {
        if (atEnd()) throw new IndexOutOfBoundsException("Stream is empty");
        return value.charAt(alreadyReadChars++);
    }

    @Override
    public boolean atEnd() {
        return value.length() == alreadyReadChars;
    }
}
